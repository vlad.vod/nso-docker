FROM ubuntu:latest

ARG NSOVER

COPY nso-$NSOVER.linux.x86_64.installer.bin /tmp/nso

RUN apt-get update;\
    apt-get install -y openssh-client default-jre-headless xsltproc vim ant make; \
    /tmp/nso /root/nso; \
    echo '. ~/nso/ncsrc' >> /root/.bashrc; \
    apt-get -y clean autoclean; \
    apt-get -y autoremove; \
    rm -rf /tmp/* /var/tmp/* /var/lib/{apt,dpkg,cache,log}/

RUN mkdir ~/nso-project; \
    /bin/bash -c "source ~/nso/ncsrc; ncs-setup --dest ~/nso-project"; \
    cp -r /root/nso/packages/neds/cisco-ios-cli-3.8 /root/nso-project/packages; \
    cp -r /root/nso/packages/neds/cisco-iosxr-cli-3.5 /root/nso-project/packages; \
    cp -r /root/nso/packages/neds/cisco-nx-cli-3.0 /root/nso-project/packages

WORKDIR /root/nso-project

EXPOSE 2022 2024 8080
