---
home: true
heroImage: /SD-WAN.png
actionText: Getting Started →
actionLink: /docker/
features:
- title: Infrastructure overview
  details: NSO docker setup & operation.  
- title: Basic configs
  details: NSO, docker, etc.
footer: Licensed | Copyright © VV
---

_build documentation for GitLab pages_

```bash
./mvnw -P gitlab-pages -pl :docs
```

_build documentation locally and start server_

```bash
./mvnw -P docs -pl :docs
```