const {version} = require('../../package.json');
const baseHref = process.env.GITLAB ? '/nso-docker/' : '/';

module.exports = {
    title: 'NSO',
    description: "Guides",
    port: 8099,
    base: baseHref,
    head: [
        ['link', {rel: 'icon', href: '/internet-security-security.png'}]
    ],
    plugins: [
        '@vuepress/theme-default'
    ],
    themeConfig: {
        logo: '/internet-security-security.png',
        sidebar: [
            {
                title: 'Docker',
                collapsable: false,
                children: [
                    '/docker/'
                ]
            },
            {
                title: 'Service',
                collapsable: false,
                children: [
                    '/service/'
                ]
            }
        ]
    }
};
