# Docker setup

## Creating Docker Image

1. Clone nso-docker project.
2. Download NSO install files for linux: [Download](https://developer.cisco.com/docs/nso/#!getting-nso/downloads)
3. Extract & copy the download (so you get the nso-5.1.0.1.linux.x86_64.installer.bin file) in the project root.
4. Build docker image:
```bash
docker build --no-cache --build-arg NSOVER=5.1.0.1 -t nso-base .
```

## Run docker Image

```bash
docker run -it -p 2022:2022 -p 2024:2024 -p 8080:8080 nso-base
```