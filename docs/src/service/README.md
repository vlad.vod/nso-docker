# Setting up Radius service

[[toc]]

Prerequisites:

:::tip
1. docker container with nso running
2. working dir = /nso-project
:::

### 1. Create devices & NCS environment running cmd in /nso-project

- Create devices:

```sh
    ncs-netsim create-network $NCS_DIR/packages/neds/cisco-ios-cli-3.0 3 c
```
		
- Create NCS environment:

```sh
    cs-setup --netsim-dir ./netsim --dest .
```

### 2. Create a new nso package

```sh
	cd nso-project/packages
	ncs-make-package --service-skeleton template simple_radius --augment /ncs:services
```

- Open the simple_radius.yang file that is found in nso-project/packages/simple_radius/src/yang:

```sh
    vi simple_radius/src/yang/simple_radius.yang
```

- Change the 'name' node to radius_server_ip and remove the dummy node:

```
 +   key radius_server_ip;
 -   key name;

    uses ncs:service-data;
    ncs:servicepoint "simple_radius";

+    leaf radius_server_ip {
-    leaf name {
      type string;
    }

-   // replace with your own stuff here
-   leaf dummy {
-     type inet:ipv4-address;
-   }
```

- Compile/make our package for NSO

```sh
	cd nso-project/packages/simple_radius/src
	make
```

_Building XML template:_

```sh
	cd ~/nso-project

	ncs-netsim start

	ncs

	ncs_cli -C -u admin

	conf

	devices device c0 config
	ios:radius server one
	address ipv4 10.0.0.1 auth-port 1812 acct-port 1813
	commit dry-run outformat xml
	end
```

- Discard changes

::: warning
'<Uncommitted changes found, commit them? [yes/no/CANCEL]> no'
:::

_"Expected result" ->_

```xml
result-xml {                                            
    local-node {                                        
        data <devices xmlns="http://tail-f.com/ns/ncs"> 
               <device>                                 
                 <name>c0</name>                        
                 <config>                               
                   <radius xmlns="urn:ios">             
                     <server>                           
                       <id>one</id>                     
                       <address>                        
                         <ipv4>                         
                           <host>10.0.0.1</host>        
                           <auth-port>1812</auth-port>  
                           <acct-port>1813</acct-port>  
                         </ipv4>                        
                       </address>                       
                     </server>                          
                   </radius>                            
                 </config>                              
               </device>                                
             </devices>                                 
    }                                                   
}                
```

### 3. Load the package into our NSO

From the nso cli issue the packages reload command:

_Back to NSO:_
```sh
    ncs_cli -C -u admin

    admin@ncs# packages reload
```
    

```
	>>> System upgrade is starting.
	>>> Sessions in configure mode must exit to operational mode.
	>>> No configuration changes can be performed until upgrade has completed.

	reload-result {
	    package simple_radius
	    result true
	}
```

- Connect to the devices and read up their configurations into NCS.

_Exit nso_cli C mode & login as:_
```sh
    ncs_cli -u admin

    request devices connect

    request devices sync-from
```

### 4. Create service (radius setup on device c0) in cli

_Exit nso_cli and re-login with -C_

```sh
 	ncs_cli -C -u admin

 	conf

 	services simple_radius 10.20.30.40

 	device c0

 	commit dry-run outformat native
```

Expected result:
```
 	native {                                                             
    	device {                                                         
        	name c0                                                      
        	data radius server one                                       
             	  address ipv4 10.20.30.40 auth-port 1812 acct-port 1813 
             	 !                                                       
    	}                                                                
	} 
```
 